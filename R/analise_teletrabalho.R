#remotes::install_local("../PNADcovidIBGE/",force = T)
library(PNADcIBGE)
library(survey)
library(ggthemes)
library(hrbrthemes)
library(viridis)

destdir <- "data/PNAD-covid19/"

pnadc19mai <- get_pnadcovid19(2020, 5,savedir = destdir)



##Variáveis que interessam para trabalho remoto
#C12 <- local que costuma trabalhar?
#C13 <- tele trabalho ou trabalho remoto?

### Trabalho remoto antes da pandemia
### C12 e C13 Sim

##0 em maio
#svyby(~C012, ~C013,pnadc19mai, svytotal,na.rm = TRUE )


teletrab_perfil_det <- svyby(~C007C,~C013,pnadc19mai,svytotal, na.rm = TRUE)

teletrab_perfil <- svyby(~C007,~C013,pnadc19mai,svytotal, na.rm = TRUE)

blargh <- subset(pnadc19mai,C013 == 1)

##
arqspnad <- dir(destdir,"*.csv", full.names = T)
dicspnad <- dir(destdir,"*.xls", full.names = T )

teletrabalho <- function(arqspnad,dicspnad) {
  pegatele <- function(arqpnad,dicpnad){
    pesq <- read_pnadcovid19(arqpnad)
    pesq <- pnadcovid19_labeller(pesq,dicpnad)
    pesq <- pnadcovid19_design(pesq)
    telet <- svyby(~C013,~C007,pesq,svytotal, na.rm = T)
     nu <- nchar(arqspnad)
     resp <- data.frame( periodo = substr(arqpnad,nu-9,nu-4),
                         tele_trabalhadores = telet[1:2])
    rm(pesq)
    gc()
   resp
  }
  resp <- data.table::rbindlist(mapply(pegatele,arqspnad,dicspnad, SIMPLIFY = F))
  resp
}

teletrab <- teletrabalho(arqspnad,dicspnad)

names(teletrab) <- c("periodo","setor","teletrabalhadores")

tirados <- unique(teletrab$setor)[c(1,8,9)]

teletrab %<>% filter(!(setor %in% tirados) & periodo != "052020")

teletrabt <- teletrab %>% pivot_wider(names_from = setor,
                                      values_from = teletrabalhadores)

teletrabprop <-
  proportions(as.array(as.matrix(teletrabt[-1])),1)

teletrabprop <- bind_cols(periodo=teletrabt$periodo,teletrabprop)

teletrabprop %<>% pivot_longer(-1,names_to = "setor", values_to = "percent")

teletrabc <- teletrab%>%left_join(teletrabprop)

ggplot(teletrabc,aes(x = periodo, y = teletrabalhadores/(1000), fill = setor))+
  geom_bar(position="stack", stat = "identity")+
  hrbrthemes::theme_ft_rc()+
  scale_fill_viridis(discrete = T)+
  geom_text(aes(label=paste0(sprintf("%1.1f", percent*100),"%")),colour = "white",
            position=position_stack(vjust=0.5),size=4)
