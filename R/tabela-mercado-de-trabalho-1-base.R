#tabela de mercado de trabalho 
#2018, 2020-02, 2020-06
##Roda script para baixar e preparar dados covid-19
#source("R/get_pnad_covid.R")

library(readxl)
library(tidyverse)
library(scales)
##Evitar notação científica
options(scipen = 100)
library(PNADcIBGE)
library(survey)

##Lendo os resultados de script microrregional com dados Pnadc
nivel_ocupformal2 <- read_csv2("data/2020-2019-12-e-5_indicador_microrregional_desocupacao.csv")

ocupacao_formal <- nivel_ocupformal2 %>% 
  select(uf,cod_mr,`Competência Declarada`,microrregiao,saldo_empregos,pia) %>% 
  group_by(uf,cod_mr,microrregiao,`Competência Declarada`) %>% 
  summarize(across(saldo_empregos:pia,sum))

ocupacao_formal <- ocupacao_formal %>% mutate(nivel_de_ocupacao=saldo_empregos/pia, nivel_de_desocupacao_formal=1-nivel_de_ocupacao)

ocupacao_formal_nac <- ocupacao_formal %>% 
  ungroup()%>%
  select(`Competência Declarada`,saldo_empregos,pia)%>%
  group_by(`Competência Declarada`)%>%
  summarize(across(saldo_empregos:pia,sum)) %>%
  mutate(nivel_de_ocupacao=saldo_empregos/pia, 
         nivel_de_desocupacao_formal=1-nivel_de_ocupacao,
         periodo = `Competência Declarada`) %>%
  select(periodo:saldo_empregos) %>%
  pivot_longer(-1,names_to="indicador",values_to="valor")

  

niv_d_df <- ggplot(ocupacao_formal_nac%>% filter(indicador == "nivel_de_desocupacao_formal") ,aes(periodo,valor))+
  geom_line()+
  theme_classic()

plotly::ggplotly(niv_d_df)

(0.5171064-0.49838)*167

n_d_n <- ocupacao_formal_nac[ocupacao_formal_nac$indicador == "nivel_de_desocupacao_formal",]$valor
linha_desocupacao <- c(n_d_n[1],n_d_n[which(unique(ocupacao_formal_nac$periodo) == "2020-02-28")],n_d_n[length(n_d_n)])



###Com dados direto da PNAD
# library(PNADcIBGE)
# library(survey)
#Variável 	Descrição
# UF 	Unidade da Federação
# V2007 	Sexo
# V2009 	Idade do morador na data de referência
# V2010 	Cor ou raça
# V3007 	Já concluiu algum outro curso de graduação?
# VD3001 	Nível de instrução mais elevado alcançado (pessoas de 5 anos ou mais de idade)
# VD4001 	Condição em relação à força de trabalho na semana de referência para pessoas de 14 anos ou mais de idade
# VD4002 	Condição de ocupação na semana de referência para pessoas de 14 anos ou mais de idade
# VD4020 	Rendimento mensal efetivo de todos os trabalhos para pessoas de 14 anos ou mais de idade (apenas para pessoas que receberam em dinheiro, produtos ou mercadorias em qualquer trabalho)
# VD4035 	Horas efetivamente trabalhadas na semana de referência em todos os trabalhos para pessoas de 14 anos ou mais de idade
ano <- 2017
trimestre <- 4


mt_sint <- function(ano,trimestre = 1) {
pnad <- get_pnadc(ano,trimestre)
sexos <- unique(pnad$variables$V2007)
etnias <- unique(pnad$variables$V2010)
pnad_negra <- pnad
pnad_negra$variables$V2010 <- gsub("Preta","Negra",as.character(pnad_negra$variables$V2010))
pnad_negra$variables$V2010 <- gsub("Parda","Negra",pnad_negra$variables$V2010)
pnad_negra$variables$V2010 <- as.factor(pnad_negra$variables$V2010)


#vars_analisar <- c("UF", "V2007", "V2009", "V2010", "VD3001", "VD4001","V4010","VD4002", "VD4020", "VD4035")
##Acerta colunas e nomes após extrapolação de sexo, etnia e variável de interesse
divi_p <- function(tabela,nomes = c("sexo","etnia","sit_ocup")){ 
  coluna <- gsub("interaction\\(.*\\)","",names(tabela))
  coluna <- as_tibble(coluna) %>% separate(1,into = nomes,sep = "\\.")
  tabela <- cbind(coluna,as_tibble(tabela)[1])
  tabela
  }

cor_sexo_oc <- svytotal(~ interaction(V2007, V2010,VD4002), pnad, na.rm = T)

cor_sexo_ft <- divi_p(cor_sexo_oc)

#Adiciona fora da força de trabalho

fora_ft <- svytotal(~interaction(V2007,V2010,VD4001 == "Pessoas fora da força de trabalho"),pnad, na.rm = T)

fft_prep <- divi_p(fora_ft)
fft_prep[fft_prep$sit_ocup == T,]$sit_ocup <- "Pessoas fora da força de trabalho"

fft_prep[fft_prep$sit_ocup == F,]$sit_ocup <- "Pessoas na força de trabalho"

cor_sexo_ft <- bind_rows(cor_sexo_ft,fft_prep)

##fazendo negros = pretos + pardos
cor_sexo_ft[cor_sexo_ft$etnia == "Preta" | cor_sexo_ft$etnia == "Parda",]$etnia <- "Negra"

#Juntando linhas correspondentes
cor_sexo_ft <- cor_sexo_ft %>% group_by(sexo,etnia,sit_ocup) %>% summarize(total = sum(total))

c_s_i <- cor_sexo_ft %>% 
filter(sit_ocup != "Pessoas na força de trabalho") %>% 
  pivot_wider(names_from = sit_ocup, values_from = total)

##Taxa de desemprego  por sexo e raca e
##Taxa de atividade




csi_totais <- tibble(sexo = "Total",etnia = "Total",as_tibble(t(colSums(c_s_i[,-1:-2]))))

c_s_i <- bind_rows(c_s_i,csi_totais)

c_s_i <- c_s_i %>% mutate(taxa_de_desemprego = `Pessoas desocupadas`/(`Pessoas ocupadas`+`Pessoas desocupadas`),
                          nivel_de_ocupacao = `Pessoas ocupadas`/(`Pessoas desocupadas`+`Pessoas ocupadas`+`Pessoas fora da força de trabalho`),
                          taxa_de_atividade = (`Pessoas ocupadas`+`Pessoas desocupadas`)/(`Pessoas desocupadas`+`Pessoas ocupadas`+`Pessoas fora da força de trabalho`))


##Taxa de informalidade
informais <- c("Empregado no setor privado sem carteira de trabalho assinada",
"Empregado no setor público sem carteira de trabalho assinada",
"Trabalhador doméstico sem carteira de trabalho assinada",
"Trabalhador familiar auxiliar",
"Conta-própria")

todascat <- unique(pnad$variables$VD4009)
formais <- as.character(todascat[!(grepl(" sem ",todascat)| grepl("Conta",todascat) | is.na(todascat))])


inform_sc <- svytotal(~interaction(V2007,V2010,VD4009 %in% informais),
                      pnad, na.rm = T)

inform_sc <- divi_p(inform_sc)

###Descartar formais pois inclui NA's pelo visto

inform_sc <- inform_sc %>% filter(sit_ocup == T)%>% transmute(sexo, etnia, informal = total)


form_sc <- svytotal(~interaction(V2007,V2010,VD4009 %in% formais),pnad,na.rm = T)

form_sc <- divi_p(form_sc)

form_sc <- form_sc %>% filter(sit_ocup == T) %>% transmute(sexo,etnia, formal = total)

informalidade <- inform_sc %>% left_join(form_sc)

##Juntar em negros = pretos e pardos
informalidade[informalidade$etnia == "Preta" | informalidade$etnia == "Parda",]$etnia <- "Negra"

informalidade <- informalidade %>% group_by(sexo,etnia)%>%summarize(across(informal:formal,sum))

isc_total <- tibble(sexo = "Total",etnia = "Total",as_tibble(t(colSums(informalidade[,-1:-2]))))

informalidade <- bind_rows(informalidade,isc_total)


informalidade$taxa_de_informalidade <- informalidade$informal/(informalidade$informal+informalidade$formal)

##Rendimento
i_renda <- svyby(~VD4020,~(I(VD4009 %in% informais)+V2007+ V2010),pnad_negra,svymean,na.rm = T)

i_renda_t <- svyby(~VD4020,~(I(VD4009 %in% informais)+I(VD4009 %in% formais)),pnad_negra,svymean, na.rm = T)

f_renda <- svyby(~VD4020,~(I(VD4009 %in% formais)+V2007+V2010),pnad_negra,svymean,na.rm = T)



## domesticas
## informalidade e volume
domesticas <- c("Trabalhador doméstico sem carteira de trabalho assinada",
                "Trabalhador doméstico com carteira de trabalho assinada" )
i_dom <- svytotal(~interaction(V2007,V2010,grepl("domés",VD4009)),
                      pnad_negra, na.rm = T)
i_dom <- divi_p(i_dom,c("sexo","etnia","domestica"))

i_dom <- i_dom[i_dom$domestica == T,]
i_dom <- i_dom%>% transmute(sexo,etnia,domestica = total)

i_dom_prop <- tibble(i_dom[1:2],domestica_prop = prop.table(i_dom$domestica))



i_dom_i <- svytotal(~interaction(V2007,V2010,grepl("doméstico s",VD4009)),
                  pnad_negra, na.rm = T)

i_dom_i <- divi_p(i_dom_i,c("sexo","etnia","domestica_informal"))

i_dom_i <- i_dom_i[i_dom_i$domestica_informal == T,]

i_dom_i <- i_dom_i%>%transmute(sexo,etnia,domestica_informal = total)

i_dom_i_prop <- tibble(i_dom_i[1:2],"domestica_informal_prop"= prop.table(i_dom_i$domestica_informal))

i_dom_i_t <- i_dom %>%
  left_join(i_dom_i) %>%
  left_join(i_dom_prop)%>%
  left_join(i_dom_i_prop)

idi_tot <- tibble(sexo="Total",etnia="Total", i_dom_i_t %>% summarize(across(starts_with("domestica"),sum)))
idi_tot$domestica_informal_prop <- idi_tot$domestica_informal/idi_tot$domestica

i_dom_i_t <- bind_rows(i_dom_i_t,idi_tot)
##renda domesticas
dom_renda_sci <- svyby(~VD4020,~(I(VD4009 %in% domesticas[1] )+V2007+V2010),pnad_negra,svymean,na.rm =T)
dom_renda_sci <- dom_renda_sci%>%select(-se) %>%transmute(domestica_informal=`I(VD4009 %in% domesticas[1])`,
                                                          sexo=V2007,etnia=V2010,renda_media_domestica_informal=VD4020)
dom_renda_sci_res <- dom_renda_sci %>% filter(domestica_informal == T) %>% select(-1)

dom_renda_tot <- svyby(~VD4020,~(I(VD4009 %in% domesticas)),pnad_negra,svymean,na.rm = T)
dom_renda_tot <- dom_renda_tot%>%select(-se) %>%transmute(domesticas_fi=`I(VD4009 %in% domesticas)`,
                                                          renda_media_domestica_total=VD4020)

dom_renda_tot_sc <- svyby(~VD4020,~(I(grepl("domé",VD4009))+V2007+V2010),pnad_negra,svymean,na.rm = T)
dom_renda_tot_sc <- dom_renda_tot_sc%>%select(-se) %>%transmute(domestica_inform_e_for=`I(grepl("domé", VD4009))`,
                                                          sexo=V2007,etnia=V2010,renda_media_domestica=VD4020)

dom_renda_tot_sct <- dom_renda_tot_sc %>% filter(domestica_inform_e_for == T)%>% select(-1)

dom_renda_tot_sct <- bind_rows(dom_renda_tot_sct,
                               tibble(sexo="Total",
                                      etnia="Total",
                                      renda_media_domestica= as.double(as_tibble(dom_renda_tot)%>%
                                        filter(domesticas_fi == T)%>%
                                        select(-1))))
dom_renda_tot_nd <- dom_renda_tot_sc %>% 
  filter(domestica_inform_e_for == F)%>% 
  select(-1) %>% 
  transmute(sexo,etnia,renda_nao_domestica = renda_media_domestica)


dom_renda_tot_nd <- bind_rows(dom_renda_tot_nd,
                               tibble(sexo="Total",
                                      etnia="Total",
                                      renda_nao_domestica = as.double(as_tibble(dom_renda_tot)%>%
                                                                         filter(domesticas_fi == F)%>%
                                                                         select(-1))))
##carga domesticas
#dom_carga_sci <- svyby(~VD4035,~(I(VD4009 %in% domesticas[1] )+V2007+V2010),pnad_negra,svymean,na.rm =T)
#dom_carga_sci <- dom_carga_sci%>%select(-se) %>%transmute(domestica_informal=`I(VD4009 %in% domesticas[1])`,
                                                          #sexo=V2007,etnia=V2010,carga_media_domestica_informal=VD4035)
#dom_carga_sci_res <- dom_carga_sci %>% filter(domestica_informal == T) %>% select(-1)

dom_carga_tot <- svyby(~VD4035,~(I(VD4009 %in% domesticas)),pnad_negra,svymean,na.rm = T)
dom_carga_tot <- dom_carga_tot%>%select(-se) %>%transmute(domesticas_fi=`I(VD4009 %in% domesticas)`,
                                                          carga_media_domestica_total=VD4035)

dom_carga_tot_sc <- svyby(~VD4035,~(I(grepl("domé",VD4009))+V2007+V2010),pnad_negra,svymean,na.rm = T)
dom_carga_tot_sc <- dom_carga_tot_sc%>%select(-se) %>%transmute(domestica_inform_e_for=`I(grepl("domé", VD4009))`,
                                                                sexo=V2007,etnia=V2010,carga_media_domestica=VD4035)

dom_carga_tot_sct <- dom_carga_tot_sc %>% filter(domestica_inform_e_for == T)%>% select(-1)

dom_carga_tot_sct <- bind_rows(dom_carga_tot_sct,
                               tibble(sexo="Total",
                                      etnia="Total",
                                      carga_media_domestica= as.double(as_tibble(dom_carga_tot)%>%
                                                                         filter(domesticas_fi == T)%>%
                                                                         select(-1))))
dom_carga_tot_nd <- dom_carga_tot_sc %>% 
  filter(domestica_inform_e_for == F)%>% 
  select(-1) %>% 
  transmute(sexo,etnia,carga_nao_domestica = carga_media_domestica)


dom_carga_tot_nd <- bind_rows(dom_carga_tot_nd,
                              tibble(sexo="Total",
                                     etnia="Total",
                                     carga_nao_domestica = as.double(as_tibble(dom_carga_tot)%>%
                                                                       filter(domesticas_fi == F)%>%
                                                                       select(-1))))
##
uberizados <- c(8321,8322,9621)
uberc <- svyby(~I(V4010 %in% uberizados | V4041 %in% uberizados),
               ~(V2007+V2010), pnad_negra, svytotal,na.rm = T)

uberc_t <- as_tibble(uberc[c(1,2,4)])

names(uberc_t) <- c("sexo","etnia","entregador")

uberc_t <- bind_rows(uberc_t,tibble(sexo="Total",etnia="Total",entregador=sum(uberc_t$entregador)))


##Renda Uber
uber_renda <- svyby(~VD4020,~(I(V4010 %in% uberizados | V4041 %in% uberizados)+ V2007+V2010),pnad_negra,svymean,na.rm = T)

uber_renda_u <- as_tibble(uber_renda[uber_renda$`I(V4010 %in% uberizados | V4041 %in% uberizados)` == T,2:4], stringsAsFactors = F)

names(uber_renda_u) <- c("sexo","etnia","renda_entregador")

uber_rt <- svyby(~VD4020,~I(V4010 %in% uberizados| V4041 %in% uberizados),pnad_negra,svymean,na.rm = T)

uber_renda_u <- bind_rows(uber_renda_u,tibble(sexo="Total",etnia="Total",renda_entregador = uber_rt[2,2]))

##Carga Horária Uber
uber_carga <- svyby(~VD4035,~(I(V4010 %in% uberizados | V4041 %in% uberizados)+ V2007+V2010),pnad_negra,svymean,na.rm = T)

uber_carga_u <- as_tibble(uber_carga[uber_carga$`I(V4010 %in% uberizados | V4041 %in% uberizados)` == T,2:4], stringsAsFactors = F)

names(uber_carga_u) <- c("sexo","etnia","carga_entregador")

uber_ct <- svyby(~VD4035,~I(V4010 %in% uberizados| V4041 %in% uberizados),pnad_negra,svymean,na.rm = T)

#quase identico
#uber_cet <-  svyby(~VD4035,~I(V4010 %in% uberizados),pnad_negra,svymean,na.rm = T)
cargas <- c(0,29,44,74,90,130)

uber_c_hist <- svyhist(~VD4035,subset(pnad_negra, V4010 %in% uberizados),na.rm=T, breaks = cargas,plot = F)

nuber_c_hist <- svyhist(~VD4035,subset(pnad_negra, !(V4010 %in% uberizados)),na.rm=T, breaks = cargas)

uber_carga_u <- bind_rows(uber_carga_u,tibble(sexo="Total",etnia="Total",carga_entregador = uber_rt[2,2]))


sintese_mt <- c_s_i %>% left_join(informalidade)%>%left_join(dom_renda_tot_sct)%>%left_join(dom_renda_tot_nd)%>%
  left_join(dom_carga_tot_sct)%>%left_join(dom_carga_tot_nd)%>%left_join(i_dom_i_t)%>%
  left_join(uberc_t)%>%left_join(uber_renda_u)%>%left_join(uber_carga_u)
rm(pnad)
gc()
sintese_mt <- tibble(sintese_mt, "ano" = ano,  trim = trimestre)
}

#selecao <- data.frame(ano = c(2017,2019,2020), trimestre = 1)
anos <- c(2017,2019,2020)

mt_sint_202006 <- mt_sint(2020,2)
sintese_mt_scp <- bind_rows(lapply(anos,mt_sint),mt_sint_202006)


pnad_covid <- get_pnadcovid(2020,7)

svytotal(~I(!(C007 %in% levels(pnad_covid$variables$C007)[9]) & (C001 == "Sim" |
                (C003 %in% levels(pnad_covid$variables$C003)[c(1)]))),
         pnad_covid,
         na.rm = T)
pnad_covid$variables$VD4001 <- as.numeric(pnad_covid$variables$A002 > 13) + ifelse( pnad_covid$variables$C001 == "Sim" |  
                                                                                pnad_covid$variables$C002 == "Sim" | pnad_covid$variables$C015 == "Sim",
                                                                              1,
                                                                              ifelse(pnad_covid$variables$C001 == "Não" &
                                                                                       pnad_covid$variables$C015 == "Não",
                                                                                     0,NA))-1
pnad_covid$variables$VD4002 <- pnad_covid$variables$VD4001 == 1 & pnad_covid$variables$C001 == "Não"

pnad_covid$variables[pnad_covid$variables$VD4002 == F & pnad_covid$variables$C001 != "Sim" & pnad_covid$variables$C001 != "Sim",]$VD4002 <- NA

pnad_covid$variables$VD4020 <- sum(pnad_covid$variables$C01012,pnad_covid$variables$C011A12,na.rm = T)

pnad_covid$variables$VD4035 <- pnad_covid$variables$C009

mt_sint_c19 <- function(ano = 2020,mes = 7) {
  pnad <- get_pnadcovid(ano,mes)
  sexos <- unique(pnad$variables$A003)
  etnias <- unique(pnad$variables$A004)
  pnad$variables$VD4001 <- as.numeric(pnad$variables$A002 > 13) + ifelse( pnad$variables$C001 == "Sim" |  
                                    pnad$variables$C002 == "Sim" | pnad$variables$C015 == "Sim" |
                                      is.na(pnad$variables$C001) | is.na(pnad$variables$C002) |
                                      is.na(pnad$variables$C015),
                                  1,
                                  NA)-1
  
  pnad$variables$VD4002 <- ifelse(pnad$variables$C001 == "Sim" | pnad$variables$C002 =="Sim", 
                                  "Pessoas ocupadas",
                                  ifelse(pnad$variables$C001 == "Não" &
                                           pnad$variables$VD4001 == 1 ,
                                         "Pessoas desocupadas",
                                         NA))
  
  
  pnad$variables$VD4020 <- sum(pnad$variables$C01012,pnad$variables$C011A12,na.rm = T)
  
  pnad$variables$VD4035 <- pnad$variables$C009
  pnad_negra <- pnad
  pnad_negra$variables$A004 <- gsub("Preta","Negra",as.character(pnad_negra$variables$A004))
  pnad_negra$variables$A004 <- gsub("Parda","Negra",pnad_negra$variables$A004)
  pnad_negra$variables$A004 <- as.factor(pnad_negra$variables$A004)
  
  
  #vars_analisar <- c("UF", "A003", "V2009", "A004", "VD3001", "VD4001","V4010","VD4002", "VD4020", "VD4035")
  ##Acerta colunas e nomes após extrapolação de sexo, etnia e variável de interesse
  divi_p <- function(tabela,nomes = c("sexo","etnia","sit_ocup")){ 
    coluna <- gsub("interaction\\(.*\\)","",names(tabela))
    coluna <- as_tibble(coluna) %>% separate(1,into = nomes,sep = "\\.")
    tabela <- cbind(coluna,as_tibble(tabela)[1])
    tabela
  }
  
  cor_sexo_oc <- svytotal(~ interaction(A003, A004,VD4002), pnad_negra, na.rm = T)
  
  cor_sexo_ft <- divi_p(cor_sexo_oc)
  
  #Adiciona fora da força de trabalho
  
  fora_ft <- svytotal(~interaction(A003,A004,VD4001 == "Pessoas fora da força de trabalho"),pnad_negra, na.rm = T)
  
  fft_prep <- divi_p(fora_ft)
  fft_prep[fft_prep$sit_ocup == T,]$sit_ocup <- "Pessoas fora da força de trabalho"
  
  fft_prep[fft_prep$sit_ocup == F,]$sit_ocup <- "Pessoas na força de trabalho"
  
  
  cor_sexo_ft <- bind_rows(cor_sexo_ft,fft_prep)
  
  c_s_i <- cor_sexo_ft %>% 
    filter(sit_ocup != "Pessoas na força de trabalho") %>% 
    pivot_wider(names_from = sit_ocup, values_from = total)
  
  ##Taxa de desemprego  por sexo e raca e
  ##Taxa de atividade
  
  
  
  
  csi_totais <- tibble(sexo = "Total",etnia = "Total",as_tibble(t(colSums(c_s_i[,-1:-2]))))
  
  c_s_i <- bind_rows(c_s_i,csi_totais)
  
  c_s_i <- c_s_i %>% mutate(taxa_de_desemprego = `Pessoas desocupadas`/(`Pessoas ocupadas`+`Pessoas desocupadas`),
                            nivel_de_ocupacao = `Pessoas ocupadas`/(`Pessoas desocupadas`+`Pessoas ocupadas`+`Pessoas fora da força de trabalho`),
                            taxa_de_atividade = (`Pessoas ocupadas`+`Pessoas desocupadas`)/(`Pessoas desocupadas`+`Pessoas ocupadas`+`Pessoas fora da força de trabalho`))
  
  
  ##Taxa de informalidade
  informais <- c("Empregado no setor privado sem carteira de trabalho assinada",
                 "Empregado no setor público sem carteira de trabalho assinada",
                 "Trabalhador doméstico sem carteira de trabalho assinada",
                 "Trabalhador familiar auxiliar",
                 "Conta-própria")
  
  todascat <- unique(pnad$variables$VD4009)
  formais <- as.character(todascat[!(grepl(" sem ",todascat)| grepl("Conta",todascat) | is.na(todascat))])
  
  
  inform_sc <- svytotal(~interaction(A003,A004,VD4009 %in% informais),
                        pnad, na.rm = T)
  
  inform_sc <- divi_p(inform_sc)
  
  ###Descartar formais pois inclui NA's pelo visto
  
  inform_sc <- inform_sc %>% filter(sit_ocup == T)%>% transmute(sexo, etnia, informal = total)
  
  
  form_sc <- svytotal(~interaction(A003,A004,VD4009 %in% formais),pnad,na.rm = T)
  
  form_sc <- divi_p(form_sc)
  
  form_sc <- form_sc %>% filter(sit_ocup == T) %>% transmute(sexo,etnia, formal = total)
  
  informalidade <- inform_sc %>% left_join(form_sc)
  
  ##Juntar em negros = pretos e pardos
  informalidade[informalidade$etnia == "Preta" | informalidade$etnia == "Parda",]$etnia <- "Negra"
  
  informalidade <- informalidade %>% group_by(sexo,etnia)%>%summarize(across(informal:formal,sum))
  
  isc_total <- tibble(sexo = "Total",etnia = "Total",as_tibble(t(colSums(informalidade[,-1:-2]))))
  
  informalidade <- bind_rows(informalidade,isc_total)
  
  
  informalidade$taxa_de_informalidade <- informalidade$informal/(informalidade$informal+informalidade$formal)
  
  ##Rendimento
  i_renda <- svyby(~VD4020,~(I(VD4009 %in% informais)+A003+ A004),pnad_negra,svymean,na.rm = T)
  
  i_renda_t <- svyby(~VD4020,~(I(VD4009 %in% informais)+I(VD4009 %in% formais)),pnad_negra,svymean, na.rm = T)
  
  f_renda <- svyby(~VD4020,~(I(VD4009 %in% formais)+A003+A004),pnad_negra,svymean,na.rm = T)
  
  
  
  ## domesticas
  ## informalidade e volume
  domesticas <- c("Trabalhador doméstico sem carteira de trabalho assinada",
                  "Trabalhador doméstico com carteira de trabalho assinada" )
  i_dom <- svytotal(~interaction(A003,A004,grepl("domés",VD4009)),
                    pnad_negra, na.rm = T)
  i_dom <- divi_p(i_dom,c("sexo","etnia","domestica"))
  
  i_dom <- i_dom[i_dom$domestica == T,]
  i_dom <- i_dom%>% transmute(sexo,etnia,domestica = total)
  
  i_dom_prop <- tibble(i_dom[1:2],domestica_prop = prop.table(i_dom$domestica))
  
  
  
  i_dom_i <- svytotal(~interaction(A003,A004,grepl("doméstico s",VD4009)),
                      pnad_negra, na.rm = T)
  
  i_dom_i <- divi_p(i_dom_i,c("sexo","etnia","domestica_informal"))
  
  i_dom_i <- i_dom_i[i_dom_i$domestica_informal == T,]
  
  i_dom_i <- i_dom_i%>%transmute(sexo,etnia,domestica_informal = total)
  
  i_dom_i_prop <- tibble(i_dom_i[1:2],"domestica_informal_prop"= prop.table(i_dom_i$domestica_informal))
  
  i_dom_i_t <- i_dom %>%
    left_join(i_dom_i) %>%
    left_join(i_dom_prop)%>%
    left_join(i_dom_i_prop)
  
  idi_tot <- tibble(sexo="Total",etnia="Total", i_dom_i_t %>% summarize(across(starts_with("domestica"),sum)))
  idi_tot$domestica_informal_prop <- idi_tot$domestica_informal/idi_tot$domestica
  
  i_dom_i_t <- bind_rows(i_dom_i_t,idi_tot)
  ##renda domesticas
  dom_renda_sci <- svyby(~VD4020,~(I(VD4009 %in% domesticas[1] )+A003+A004),pnad_negra,svymean,na.rm =T)
  dom_renda_sci <- dom_renda_sci%>%select(-se) %>%transmute(domestica_informal=`I(VD4009 %in% domesticas[1])`,
                                                            sexo=A003,etnia=A004,renda_media_domestica_informal=VD4020)
  dom_renda_sci_res <- dom_renda_sci %>% filter(domestica_informal == T) %>% select(-1)
  
  dom_renda_tot <- svyby(~VD4020,~(I(VD4009 %in% domesticas)),pnad_negra,svymean,na.rm = T)
  dom_renda_tot <- dom_renda_tot%>%select(-se) %>%transmute(domesticas_fi=`I(VD4009 %in% domesticas)`,
                                                            renda_media_domestica_total=VD4020)
  
  dom_renda_tot_sc <- svyby(~VD4020,~(I(grepl("domé",VD4009))+A003+A004),pnad_negra,svymean,na.rm = T)
  dom_renda_tot_sc <- dom_renda_tot_sc%>%select(-se) %>%transmute(domestica_inform_e_for=`I(grepl("domé", VD4009))`,
                                                                  sexo=A003,etnia=A004,renda_media_domestica=VD4020)
  
  dom_renda_tot_sct <- dom_renda_tot_sc %>% filter(domestica_inform_e_for == T)%>% select(-1)
  
  dom_renda_tot_sct <- bind_rows(dom_renda_tot_sct,
                                 tibble(sexo="Total",
                                        etnia="Total",
                                        renda_media_domestica= as.double(as_tibble(dom_renda_tot)%>%
                                                                           filter(domesticas_fi == T)%>%
                                                                           select(-1))))
  dom_renda_tot_nd <- dom_renda_tot_sc %>% 
    filter(domestica_inform_e_for == F)%>% 
    select(-1) %>% 
    transmute(sexo,etnia,renda_nao_domestica = renda_media_domestica)
  
  
  dom_renda_tot_nd <- bind_rows(dom_renda_tot_nd,
                                tibble(sexo="Total",
                                       etnia="Total",
                                       renda_nao_domestica = as.double(as_tibble(dom_renda_tot)%>%
                                                                         filter(domesticas_fi == F)%>%
                                                                         select(-1))))
  ##carga domesticas
  #dom_carga_sci <- svyby(~VD4035,~(I(VD4009 %in% domesticas[1] )+A003+A004),pnad_negra,svymean,na.rm =T)
  #dom_carga_sci <- dom_carga_sci%>%select(-se) %>%transmute(domestica_informal=`I(VD4009 %in% domesticas[1])`,
  #sexo=A003,etnia=A004,carga_media_domestica_informal=VD4035)
  #dom_carga_sci_res <- dom_carga_sci %>% filter(domestica_informal == T) %>% select(-1)
  
  dom_carga_tot <- svyby(~VD4035,~(I(VD4009 %in% domesticas)),pnad_negra,svymean,na.rm = T)
  dom_carga_tot <- dom_carga_tot%>%select(-se) %>%transmute(domesticas_fi=`I(VD4009 %in% domesticas)`,
                                                            carga_media_domestica_total=VD4035)
  
  dom_carga_tot_sc <- svyby(~VD4035,~(I(grepl("domé",VD4009))+A003+A004),pnad_negra,svymean,na.rm = T)
  dom_carga_tot_sc <- dom_carga_tot_sc%>%select(-se) %>%transmute(domestica_inform_e_for=`I(grepl("domé", VD4009))`,
                                                                  sexo=A003,etnia=A004,carga_media_domestica=VD4035)
  
  dom_carga_tot_sct <- dom_carga_tot_sc %>% filter(domestica_inform_e_for == T)%>% select(-1)
  
  dom_carga_tot_sct <- bind_rows(dom_carga_tot_sct,
                                 tibble(sexo="Total",
                                        etnia="Total",
                                        carga_media_domestica= as.double(as_tibble(dom_carga_tot)%>%
                                                                           filter(domesticas_fi == T)%>%
                                                                           select(-1))))
  dom_carga_tot_nd <- dom_carga_tot_sc %>% 
    filter(domestica_inform_e_for == F)%>% 
    select(-1) %>% 
    transmute(sexo,etnia,carga_nao_domestica = carga_media_domestica)
  
  
  dom_carga_tot_nd <- bind_rows(dom_carga_tot_nd,
                                tibble(sexo="Total",
                                       etnia="Total",
                                       carga_nao_domestica = as.double(as_tibble(dom_carga_tot)%>%
                                                                         filter(domesticas_fi == F)%>%
                                                                         select(-1))))
  ##
  uberizados <- c(8321,8322,9621)
  uberc <- svyby(~I(V4010 %in% uberizados | V4041 %in% uberizados),
                 ~(A003+A004), pnad_negra, svytotal,na.rm = T)
  
  uberc_t <- as_tibble(uberc[c(1,2,4)])
  
  names(uberc_t) <- c("sexo","etnia","entregador")
  
  uberc_t <- bind_rows(uberc_t,tibble(sexo="Total",etnia="Total",entregador=sum(uberc_t$entregador)))
  
  
  ##Renda Uber
  uber_renda <- svyby(~VD4020,~(I(V4010 %in% uberizados | V4041 %in% uberizados)+ A003+A004),pnad_negra,svymean,na.rm = T)
  
  uber_renda_u <- as_tibble(uber_renda[uber_renda$`I(V4010 %in% uberizados | V4041 %in% uberizados)` == T,2:4], stringsAsFactors = F)
  
  names(uber_renda_u) <- c("sexo","etnia","renda_entregador")
  
  uber_rt <- svyby(~VD4020,~I(V4010 %in% uberizados| V4041 %in% uberizados),pnad_negra,svymean,na.rm = T)
  
  uber_renda_u <- bind_rows(uber_renda_u,tibble(sexo="Total",etnia="Total",renda_entregador = uber_rt[2,2]))
  
  ##Carga Horária Uber
  uber_carga <- svyby(~VD4035,~(I(V4010 %in% uberizados | V4041 %in% uberizados)+ A003+A004),pnad_negra,svymean,na.rm = T)
  
  uber_carga_u <- as_tibble(uber_carga[uber_carga$`I(V4010 %in% uberizados | V4041 %in% uberizados)` == T,2:4], stringsAsFactors = F)
  
  names(uber_carga_u) <- c("sexo","etnia","carga_entregador")
  
  uber_ct <- svyby(~VD4035,~I(V4010 %in% uberizados| V4041 %in% uberizados),pnad_negra,svymean,na.rm = T)
  
  #quase identico
  #uber_cet <-  svyby(~VD4035,~I(V4010 %in% uberizados),pnad_negra,svymean,na.rm = T)
  cargas <- c(0,29,44,74,90,130)
  
  uber_c_hist <- svyhist(~VD4035,subset(pnad_negra, V4010 %in% uberizados),na.rm=T, breaks = cargas,plot = F)
  
  nuber_c_hist <- svyhist(~VD4035,subset(pnad_negra, !(V4010 %in% uberizados)),na.rm=T, breaks = cargas)
  
  uber_carga_u <- bind_rows(uber_carga_u,tibble(sexo="Total",etnia="Total",carga_entregador = uber_rt[2,2]))
  
  
  sintese_mt <- c_s_i %>% left_join(informalidade)%>%left_join(dom_renda_tot_sct)%>%left_join(dom_renda_tot_nd)%>%
    left_join(dom_carga_tot_sct)%>%left_join(dom_carga_tot_nd)%>%left_join(i_dom_i_t)%>%
    left_join(uberc_t)%>%left_join(uber_renda_u)%>%left_join(uber_carga_u)
  rm(pnad)
  rm(pnad_negra)
  gc()
  sintese_mt <- tibble(sintese_mt, "ano" = ano,  trim = trimestre)
}

##Complemento renda tinha esquecido

pegarenda <- function(ano = 2020, trimestre = 2) {

  retx <- tibble(ano = 2017:2020, txt = c(rep("_20190729.txt",3),".txt"))
  pnad_negra <- read_pnadc(paste0("data/pnadc/PNADC_",sprintf("%02d",trimestre),ano,retx[retx$ano == ano,2]),
                           "data/pnadc/input_PNADC_trimestral.sas")
  pnad_negra <- pnadc_labeller(pnad_negra, dictionary.file = "data/pnadc/dicionario_PNADC_microdados_trimestral.xls")
  pnad_negra <- pnadc_design(pnad_negra)
  pnad_negra$variables$V2010 <- gsub("Preta","Negra",as.character(pnad_negra$variables$V2010))
  pnad_negra$variables$V2010 <- gsub("Parda","Negra",pnad_negra$variables$V2010)
  pnad_negra$variables$V2010 <- as.factor(pnad_negra$variables$V2010)
  ##Taxa de informalidade
  informais <- c("Empregado no setor privado sem carteira de trabalho assinada",
                 "Empregado no setor público sem carteira de trabalho assinada",
                 "Trabalhador doméstico sem carteira de trabalho assinada",
                 "Trabalhador familiar auxiliar",
                 "Conta-própria")
  
  todascat <- unique(pnad_negra$variables$VD4009)
  formais <- as.character(todascat[!(grepl(" sem ",todascat)| grepl("Conta",todascat) | is.na(todascat))])
  
   
 i_renda <- svyby(~VD4020,~(I(VD4009 %in% informais)+V2007+ V2010),pnad_negra,svymean,na.rm = T)

 i_renda_t <- svyby(~VD4020,~(I(VD4009 %in% informais)+I(VD4009 %in% formais)),pnad_negra,svymean, na.rm = T)

 f_renda <- svyby(~VD4020,~(I(VD4009 %in% formais)+V2007+V2010),pnad_negra,svymean,na.rm = T)
 
 
 rendas <- list(i_renda,i_renda_t,f_renda, "anoetrim" = paste0(ano,sprintf(fmt = "%02d",trimestre*3)))
}

geralpr <- function(ano = 2020, trimestre = 2) {
  
  retx <- tibble(ano = 2017:2020, txt = c(rep("_20190729.txt",3),".txt"))
  pnad_negra <- read_pnadc(paste0("data/pnadc/PNADC_",sprintf("%02d",trimestre),ano,retx[retx$ano == ano,2]),
                           "data/pnadc/input_PNADC_trimestral.sas")
  pnad_negra <- pnadc_labeller(pnad_negra, dictionary.file = "data/pnadc/dicionario_PNADC_microdados_trimestral.xls")
  pnad_negra <- pnadc_design(pnad_negra)
  pnad_negra$variables$V2010 <- gsub("Preta","Negra",as.character(pnad_negra$variables$V2010))
  pnad_negra$variables$V2010 <- gsub("Parda","Negra",pnad_negra$variables$V2010)
  pnad_negra$variables$V2010 <- as.factor(pnad_negra$variables$V2010)
  
  g_renda <- svyby(~VD4020, ~I(V2010), pnad_negra,svymean,na.rm = T)
  g_renda <- list(g_renda, "anoetrim" = paste0(ano,sprintf("%02d",trimestre*3)))
}

sint_ult_renda <- pegarenda(2020,2)

sintese_rendas <- rbind(lapply(anos,pegarenda,trimestre = 1),sint_ult_renda)

sintese_rendas_arr <-  rbind(sintese_rendas[[1]][[1]][,-5],
                             sintese_rendas[[3]][[1]][,-5],
                             sintese_rendas[[5]][[1]][,-5],
                             sintese_rendas[[2]][,-5])
sintese_rendas_arr$ano <- as.numeric(sapply(c(201703,201903,202003,202006),rep,20))

sintese_rendas_neg <- sintese_rendas_arr %>% filter(`I(VD4009 %in% informais)` == T, V2010 %in% c("Branca","Negra"))%>% pivot_wider(names_from = ano,values_from = VD4020)

sintese_rendas_arr_tot <-  rbind(sintese_rendas[[1]][[2]][,-4],
                                 sintese_rendas[[3]][[2]][,-4],
                                 sintese_rendas[[5]][[2]][,-4],
                                 sintese_rendas[[4]][,-4])
sintese_rendas_arr_tot$ano <- as.numeric(sapply(c(201703,201903,202003,202006),rep,4))
sintese_rendas_arr_tot <- sintese_rendas_arr_tot %>% filter(`I(VD4009 %in% informais)` +`I(VD4009 %in% formais)` == 1) %>%
  pivot_wider(names_from = ano, values_from = VD4020)

sintese_rendas_arr_tot <- bind_cols(sit_ocup = c("informais","formais"),sintese_rendas_arr_tot[,-1:-2])
  
write_csv2(sintese_rendas_arr_tot,"data/2020-2017-renda-por-formalidade.csv")

gult_r <- geralpr()
g_renda_sint <- rbind(lapply(anos,geralpr, trimestre = 1),gult_r)

g_renda_sint_arr <- rbind(g_renda_sint[[1]][[1]][,-3],g_renda_sint[[3]][[1]][,-3],g_renda_sint[[5]][[1]][,-3],g_renda_sint[[6]][,-3])

g_renda_sint_arr$ano <- as.numeric(sapply(c(201703,201903,202003,202006),rep,5))
