### Construção a partir da aula anterior com foco em população e tabelas


### Elementos da aula passada:
# 1) Como carregar e utilizar:
# - outros scripts
# - pacotes não disponíveis no cran
# 2) Como adicionar colunas e linhas, calculando indicadores
# - Com base
# - com tidyverse

### Elementos desta aula
# 1) Mais alguns exemplos - agregar por grupo - base
# 2) Tabelas - exemplos .com data.table e intro a tibbles
# 3) indicador da tabela de população e tabelas do CAGED
# 4) apresentação mais formal do tidyverse



### Pacotes necessários / sugeridos
library(devtools)
library(munipopsbr)
library(data.table)
library(tidyr)
library(dplyr)
library(stringr)
library(magrittr) ## poderiam ser 4 substituídos por library(tidyverse)
library(readxl)
library(RSIDRA)
##Carreguemos alguns dados

cageds <- read.csv2("data/07_caged-2018-2019.csv")


#munipopsbr::popmunicipal(2019,2020)
populacoes <- readRDS("data/07_populacao_municipal.rds")

caged_2019_comp <- readRDS("data/08_dados_caged_2019.rds")

caged_2020_comp <- readRDS("data/08_dados_caged_2020_05.rds")

##Dicionários de 2020
nc_arq_dic <- "data/Layout Novo Caged Movimentação.xlsx"

dicionarios <- readxl::excel_sheets(nc_arq_dic)

regioes <- read_xlsx(nc_arq_dic,sheet = "região")


uefes <- read_xlsx(nc_arq_dic,sheet = "uf")


municipios <- read_xlsx(nc_arq_dic,sheet = "município")



secoes <- read_xlsx(nc_arq_dic,sheet = "seção")


categoria <- read_xlsx(nc_arq_dic,sheet = "categoria")


sexos <- read_xlsx(nc_arq_dic, sheet = "sexo")


etnias <- read_xlsx(nc_arq_dic,sheet = "raçacor")


#### Mais exemplos  com R-base e tidyverse para adicionar linhas
#e colunas

###Criar totais por linha
cageds <- cageds %>%
  group_by(competencia_declarada,tipo_de_trabalho) %>%
  summarise(saldo_mov=sum(saldo_mov))

cageds <- cageds %>% 
  pivot_wider(names_from = tipo_de_trabalho,
              values_from = saldo_mov)

###Como vocês criariam o totais por linha 
cageds$Total <- rowSums(cageds[,-1], na.rm = TRUE)


### Outras formas com R Base de fazer "resumos" por grupo
###
##
cageds$ano <- as.numeric(substr(cageds$competencia_declarada,1,4))

cagedstotais <- aggregate(saldo_mov ~ substr(competencia_declarada,1,4),
                          cageds, sum)

cagedstotais <- aggregate(cageds$saldo_mov,by=list(substr(cageds$competencia_declarada,1,4),
                                  cageds$tipo_de_trabalho),sum)

cagedstotais <- xtabs(saldo_mov ~ substr(competencia_declarada,1,4),cageds)


### Exercícios para pausa
#
cageds$ano <- as.numeric(substr(cageds$competencia_declarada,1,4))
#Criem a coluna mês de cageds
cageds$`mês` <- as.numeric(substr(cageds$competencia_declarada,5,6))
### Criar colunas com números índices
# Dos saldos de movimentação por tipo de trabalho
# (201812 = 100)
##Ou seja multiplicar por 100 
#e dividir todas as linhas pela linha 201812 correspondente, 

num_indice_col_cageds <-  function(coluna){
  100*cageds[,coluna]/as.numeric(cageds[cageds$competencia_declarada == 201812,coluna])
}

cagedsindices <- bind_cols(sapply(4:6,num_indice_col_cageds))

cagedsindices <- cbind(cageds$competencia_declarada,cagedsindices)

### Vejamos alguns totais por subgrupos




### Com R Base

### com tidyverse

postos_por_sexo_e_etnia <- caged_2020_comp %>%
  group_by(sexo, `raçacor`) %>%
  summarise(empregos_liq_criados = sum(saldomovimentação))
### Descubramos o que é cada 1

postos_por_sexo_e_etnia <- postos_por_sexo_e_etnia %>%
  left_join(etnias, by = c("raçacor" = "Código"))

postos_por_sexo_e_etnia <- postos_por_sexo_e_etnia %>%
  left_join(sexos, by = c("sexo" = "Código"))

postos_por_sexo_e_etnia <- postos_por_sexo_e_etnia %>%
  mutate(sexo = Descrição.y, raçacor = Descrição.x) %>%
  select(sexo,raçacor,empregos_liq_criados)


###Pasmem, a crise está bem pesada, destruição geral de empregos

#Recuperamos as estimativas de população por cor
source("scripts/04_caged_e_pops_comp_cores.R")

#No objeto popcor19 estão os totais por cor estimados

#Vamos fazer uma estimativa de quantos postos de trabalho foram destruídos
#Para cada posto destruído para homens e mulheres negros, logo já balanceando pela diferença
#populacional

#Primeiro passo, criar coluna de magnitude em relação a mulher negra e a homem negro

num_mulheres_negras <- as.numeric(popcor19[popcor19$`Cor ou raça` == "Preta" &
                                             popcor19$Sexo == "Mulheres",]$Valor)
num_homens_negros <- as.numeric(popcor19[popcor19$`Cor ou raça` == "Preta" &
                                             popcor19$Sexo == "Homens",]$Valor)

popcor19 <- popcor19 %>% mutate(magnitude_rel_a_negra = Valor/num_mulheres_negras,
                                magnitude_rel_ao_negro = Valor/num_homens_negros,
)

popcor19 <- popcor19 %>% mutate(Sexo = str_replace(Sexo,"Homens","Homem"), 
                                Sexo = str_replace(Sexo,"Mulheres","Mulher"))

popcor19 <- popcor19 %>% select(-1)

#Finalmente as proporções finais

tot_empregos_destruidos_negras <- as.numeric(postos_por_sexo_e_etnia %>% 
                                            ungroup()%>%
                                            filter(sexo == "Mulher", raçacor == "Preta")%>%
                                            select(empregos_liq_criados))

tot_empregos_destruidos_negros <- as.numeric(postos_por_sexo_e_etnia %>% 
                                            ungroup()%>%
                                            filter(sexo == "Homem", raçacor == "Preta")%>%
                                            select(empregos_liq_criados))

diferencas_etnias <- postos_por_sexo_e_etnia %>%
  left_join(popcor19, by = c( "raçacor" = "Cor ou raça", "sexo" = "Sexo"))


### Vamos pegar o número então relativo

diferencas_etnias <- diferencas_etnias %>%
  mutate(destruidos_rel_negros = 
           empregos_liq_criados/
           magnitude_rel_ao_negro/
           tot_empregos_destruidos_negros,
         destruidos_rel_negras = 
           empregos_liq_criados/
           magnitude_rel_a_negra/
           tot_empregos_destruidos_negras
         )

###Aqueles que conseguiram estar empregados negros, 
###Foram melhores em manter seus empregos
###Hipóteses explicativas tentativas
###1) trabalham mais/melhor
###2) círculo social de vínculo possível <- relação cor empregado/empregador?

sum(diferencas_etnias$empregos_liq_criados)

